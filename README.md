# CTA dependencies

Dependencies of CTA which aren't present in public repositories

## Generate RPMS

We generate the RPMs through the pipeline and store them in the artifacts of the jobs. To generate new RPMs one must update the `.gitlab-ci.yml`. For testing one must run a manual pipeline with the desired configuration and download the artifacts, doing changes in a branch and modifying the CI file is an alternative.

- `EPOCH`: Epoch to for all the generated RPMs. Must be updated incrementally and never removed.
- MHVTL:
  - `MHVTL_SRC_REPO`: cloned repository to build the RPMs. Defaults to main mhvtl repo. Can be set to a fork to quickly test change before they make it to the ustream repo.
  - `MHVTL_SOURCE`: either `tag` or `commit`. Default: `tag`
  - `MHVTL_COMMIT`: commit to checkout when using `commit` as a source. Default `%{nil}`, do not change if not using the `commit` source.
  - `MHVTL_TAG`: tag to build the RPMs from. Defaults to latest tested tag and in use in CI and development environments.
